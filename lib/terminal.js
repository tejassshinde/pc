var Promise = require('promise');

// database connection
var MSql = require('./../routes/server').MSql; // MariaDb Object
var Server = require('./../routes/server').Srv; // Server Datatype
var _server = new Server()
var panaesha_db = new MSql("panaesha");
var Mail = require('./../lib/mail').Mail;
var _mail = new Mail();

var Terminal = function () { };


//=======================List Order

Terminal.prototype.order_list = function (data, callback) {
    var query = `call order_list("${data.sid}")`
    //console.log(query)

    panaesha_db.query(query, function (err, rows) {
       // console.log(err)
       // console.log(rows)
        callback(err, rows)
    })
}

//=======================New Order

Terminal.prototype.newOrder = function (data, callback) {
    var query = `call newOrder2("${data.sid}","${data.symbol_id}"
    ,"${data.order_type}","${data.order_qty}","${data.order_limit_rate}")`
    //console.log(query)

    panaesha_db.query(query, function (err, rows) {
        console.log(err)
        console.log(rows)
        callback(err, rows)
    })
}
//============================ Forgot Password

Terminal.prototype.ForgotPassword = function (data, callback) {
    var query = `call emailVerify("${data.email}")`
    console.log(query)

    panaesha_db.query(query, function (err, rows) {
        if (err) {
            console.log(err)
        } else {
            console.log("Email id here: " + rows[0][0]["email"]);
            var doc = { "email": rows[0][0]["email"],
            "uid": rows[0][0]["uid"],
            "name": rows[0][0]["uname"] };

            _mail.resetPassword(doc, function (err, res) {
                console.log(err);
                console.log(res);
                console.log(res.email);

            })
        }

        callback(err, rows)
    })
}

//============================ reset Password

Terminal.prototype.resetPassword = function (data, callback) {
    var query = `call resetPassword("${data.token}","${data.newPassword}")`
    console.log(query)

    panaesha_db.query(query, function (err, rows) {
        console.log(err)
        console.log(rows)
        callback(err, rows)
    })
}

//============================ Change Password

Terminal.prototype.changePassword = function (data, callback) {
    var query = `call changePassword("${data.sid}",
    "${data.oldPassword}","${data.newPassword}")`
    console.log(query)

    panaesha_db.query(query, function (err, rows) {
        console.log(err)
        console.log(rows)
        callback(err, rows)
    })
}

//=======================List Tick

Terminal.prototype.showTick = function (data, callback) {
    var query = `call showTick("${data.sid}")`
    //console.log(query)

    panaesha_db.query(query, function (err, rows) {
       // console.log(err)
//        console.log(rows)
        callback(err, rows)
    })
}

//=======================List Trade

Terminal.prototype.listTrade = function (data, callback) {
    var query = `call listTrade("${data.sid}")`
    //console.log(query)

    panaesha_db.query(query, function (err, rows) {
       // console.log(err)
       // console.log(rows)
        callback(err, rows)
    })
}

//======================= netposition

Terminal.prototype.netPosition = function (data, callback) {
    var query = `call netPosition("${data.sid}")`
    //console.log(query)

    panaesha_db.query(query, function (err, rows) {
       // console.log(err)
       // console.log(rows)
        callback(err, rows)
    })
}

//======================= Cancel Order

Terminal.prototype.cancelPending = function (data, callback) {
    var query = `call order_cancel_pending("${data.sid}","${data.order_id}")`
    console.log(query)

    panaesha_db.query(query, function (err, rows) {
        console.log(err)
        console.log(rows)
        callback(err, rows)
    })
}

//======================= Accept TnC

Terminal.prototype.acceptTnc = function (data, callback) {
    var query = `call accept_tnc("${data.sid}")`
    console.log(query)

    panaesha_db.query(query, function (err, rows) {
        console.log(err)
        console.log(rows)
        callback(err, rows)
    })
}

//=======================New Order

Terminal.prototype.newOrderDealer = function (data, callback) {
    var query = `call newOrderDealer("${data.sid}","${data.symbol_id}"
    ,"${data.order_type}","${data.order_qty}","${data.order_limit_rate}","${data.client_code}")`
    console.log(query)

    panaesha_db.query(query, function (err, rows) {
        console.log(err)
        console.log(rows)
        callback(err, rows)
    })
}

//=======================New Order

Terminal.prototype.checkClientCode = function (data, callback) {
    var query = `call checkClientCode("${data.client_code}")`
    console.log(query)

    panaesha_db.query(query, function (err, rows) {
        callback(err, rows)
    })
}

//=======================New Order tOtpAuth

Terminal.prototype.brokerageList = function (data, callback) {
    var query = `call brokerage_list("${data.sid}")`
    console.log(query)

    panaesha_db.query(query, function (err, rows) {
        console.log(rows)
        console.log(err)
        callback(err, rows)
    })
}
//======================= tOtpAuth
Terminal.prototype.tOtpAuth = function (data, callback) {
    var query = `call totp_client_auth("${data.pcexKey}", "${data.contact}")`
    console.log(query)

    panaesha_db.query(query, function (err, rows) {
        console.log(rows)
        console.log(err)
        callback(err, rows)
    })
}



exports.Terminal = Terminal; 
