var Promise = require('promise');

// database connection
var MSql = require('./../routes/server').MSql; // MariaDb Object
var Server = require('./../routes/server').Srv; // Server Datatype
var _server=new Server()
var panaesha_db = new MSql("panaesha");

var Members=function(){};




//=========================Add Broker

Members.prototype.newBroker=function(data,callback){
    var query=`call newBroker("${data.sid}","${data.companyname}","${data.fname}","${data.lname}",
    "${data.address1}","${data.address2}"
    ,"${data.city}","${data.state}","${data.pincode}","${data.country}"
    ,"${data.contact}","${data.email}",
    "${data.pan}" , "${data.aadhar}" , "${data.gst}" ,
    "${data.swift}" , "${data.bank}" , "${data.bank_ac_no}" , "${data.ifsc}",
    "${data.fcc}" , "${data.btc}" , "${data.eth}"
    )`
    console.log(query)
    
    panaesha_db.query(query,function(err,rows){
            console.log(err)
            console.log(rows)
            callback(err,rows)
    })
}


//=========================Broker list

Members.prototype.broker_list =function(data,callback){
    var query=`call broker_list("${data.sid}")`
    console.log(query)
    
    panaesha_db.query(query,function(err,rows){
            console.log(err)
            console.log(rows)
            callback(err,rows)
    })
}
//=========================Add Broker

Members.prototype.newRm=function(data,callback){
    var query=`call newRmanager("${data.sid}","${data.companyname}","${data.fname}","${data.lname}",
    "${data.address1}","${data.address2}"
    ,"${data.city}","${data.state}","${data.pincode}","${data.country}"
    ,"${data.contact}","${data.email}"
    )`
    console.log(query)
    
    panaesha_db.query(query,function(err,rows){
            console.log(err)
            console.log(rows)
            callback(err,rows)
    })
}


//=========================Broker list

Members.prototype.listRm =function(data,callback){
    var query=`call rManager_list("${data.sid}")`
    console.log(query)
    
    panaesha_db.query(query,function(err,rows){
            console.log(err)
            console.log(rows)
            callback(err,rows)
    })
}
//=========================Broker list

Members.prototype.verifyRmCode =function(data,callback){
    var query=`call verify_rm_code("${data.rm_code}")`
    console.log(query)
    
    panaesha_db.query(query,function(err,rows){
            console.log(err)
            console.log(rows)
            callback(err,rows)
    })
}
//=========================new referal Transaction

Members.prototype.newTransaction =function(data,callback){
    var query=`call new_referal_transaction("${data.rm_code}",'${JSON.stringify(data.transactionDetails)}')`
    console.log(query)
    
    panaesha_db.query(query,function(err,rows){
            console.log(err)
            console.log(rows)
            callback(err,rows)
    })
}





exports.Members = Members; 
