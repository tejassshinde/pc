var Promise = require('promise');

// database connection
var MSql = require('./../routes/server').MSql; // MariaDb Object
var Server = require('./../routes/server').Srv; // Server Datatype
var _server = new Server()
var panaesha_db = new MSql("panaesha");

var Brokers = function () { };



//=========================Add Client

Brokers.prototype.newClient = function (data, callback) {
    var query = `call newClient("${data.sid}","${data.companyname}","${data.fname}","${data.lname}",
    "${data.address1}","${data.address2}"
    ,"${data.city}","${data.state}","${data.pincode}","${data.country}"
    ,"${data.contact}","${data.email}",
     "${data.pan}" , "${data.aadhar}" , "${data.gst}" ,
     "${data.swift}" , "${data.bank}" , "${data.bank_ac_no}" , "${data.ifsc}",
     "${data.fcc}" , "${data.btc}" , "${data.eth}"
    )`
    console.log(query);

    panaesha_db.query(query, function (err, rows) {
        console.log(err)
        console.log(rows)
        callback(err, rows)
    })
}




//=========================List Client

Brokers.prototype.client_list = function (data, callback) {
    var query = `call client_list("${data.sid}")`
    console.log(query)

    panaesha_db.query(query, function (err, rows) {
        console.log(err)
        //console.log(rows)
        callback(err, rows)
    })
}

//=========================Add Dealer

Brokers.prototype.newDealer = function (data, callback) {
    var query = `call newDealer("${data.sid}","${data.companyname}","${data.fname}","${data.lname}",
    "${data.address1}","${data.address2}"
    ,"${data.city}","${data.state}","${data.pincode}","${data.country}"
    ,"${data.contact}","${data.email}"
    )`
    console.log(query)

    panaesha_db.query(query, function (err, rows) {
        console.log(err)
        console.log(rows)
        callback(err, rows)
    })
}


//=========================List Dealer

Brokers.prototype.dealer_list = function (data, callback) {
    var query = `call dealer_list("${data.sid}")`
    console.log(query)

    panaesha_db.query(query, function (err, rows) {
        console.log(err)
        console.log(rows)
        callback(err, rows)
    })
}

//=========================search cCode

Brokers.prototype.searchClient = function (data, callback) {
    var query = `call searchClient("${data.sid}","${data.cCode}")`
    console.log(query)

    panaesha_db.query(query, function (err, rows) {
        console.log(err)
        console.log(rows)
        callback(err, rows)
    })
}


exports.Brokers = Brokers; 
