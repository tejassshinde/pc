var http = require("https");
var Promise = require('promise');

// database connection
var MSql = require('./../routes/server').MSql; // MariaDb Object
var Server = require('./../routes/server').Srv; // Server Datatype
var _server = new Server()
var panaesha_db = new MSql("panaesha");
var Mail = function () { };
//=======================================================
Mail.prototype.forgotPassword = function (data, callback) {
    var query = `call emailVerify("${data.email}")`
    console.log(query)
    var that = this;
    panaesha_db.query(query, function (err, rows) {
        if (err) {
            console.log(err)
        } else {
            console.log("Email id here: " + rows[0][0]["email"]);
            var doc = {
                "email": rows[0][0]["email"],
                "uid": rows[0][0]["uid"],
                "name": rows[0][0]["uname"],
                "role": rows[0][0]["role"],
              


            };

            console.log(doc);

            that.resetPassword(doc, function (err, res) {
                console.log(err);
                console.log(res);
                //  console.log(res.email);

            })
        }

        callback(err, "Mail Sent to " + rows[0][0]["email"]);
    });
}



//==============================Reset Password
Mail.prototype.resetPassword = function (data, callback) {

    var options = {
        "method": "POST",
        "hostname": "api.sendgrid.com",
        "port": null,
        "path": "/v3/mail/send",
        "headers": {
            "authorization": "Bearer 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",
            "content-type": "application/json"
        }
    };
    var req = http.request(options, function (res) {
        var chunks = [];

        res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        res.on("end", function () {
            var body = Buffer.concat(chunks);
            console.log(body.toString());
        });
    });

    req.write(JSON.stringify({
        template_id: 'd9c7123d-b01e-4788-94ab-bc14e5e7d7ea', personalizations:
            [{
                to: [{ email: data.email, name: data.name }],
                "substitutions": {
                    "%email%": data.email,
                    "%name%": data.name,
                    "%uid%": data.uid,
                    "%role%": data.role
                    
                },
                subject: 'Reset your password.'
            }],
        from: { email: 'pcex@panaesha.com', name: 'Team PCEX' },
        reply_to: { email: 'sam.smith@example.com', name: 'Sam Smith' },
        subject: '',
        content:
            [{
                type: 'text/html',
                value: '<html><p>Hello</p></html>'
            }]
    }));
    req.end();
    callback(null, "done")

}
//======================================== Validate Email

Mail.prototype.validate = function (data, callback) {
    var query = `call validateEmail("${data.e}","${data.i}","${data.r}")`
    console.log(query)

    panaesha_db.query(query, function (err, rows) {
        // console.log(err)
        // console.log(rows)
        callback(err, rows)
    })
}


//=======================================================
Mail.prototype.newUser = function (data, callback) {
    var query = `call emailVerify("${data.email}")`
    console.log(query)
    var that = this;
    panaesha_db.query(query, function (err, rows) {
        if (err) {
            console.log(err)
        } else {
            console.log("Email id here: " + rows[0][0]["email"]);
            var doc = {
                "email": rows[0][0]["email"],
                "uid": rows[0][0]["uid"],
                "name": rows[0][0]["uname"],
                "role": rows[0][0]["role"],
                "fname": data.fname,
                "lname": data.lname,
                "address1": data.address1,
                "address2": data.address2,
                "city": data.city,
                "state": data.state,
                "pincode": data.pincode,
                "country": data.country,
                "contact": data.contact,
                "companyname":data.companyname


            };

            console.log(doc);

            that.confirmEmail(doc, function (err, res) {
                console.log(err);
                console.log(res);
                //  console.log(res.email);

            })
        }

        callback(err, "Mail Sent to " + rows[0][0]["email"]);
    });
}



//============================== Confirm Email
Mail.prototype.confirmEmail = function (data, callback) { //here is function

    var options = {
        "method": "POST",
        "hostname": "api.sendgrid.com",
        "port": null,
        "path": "/v3/mail/send",
        "headers": {
            "authorization": "Bearer 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",
            "content-type": "application/json"
        }
    };
    var req = http.request(options, function (res) {
        var chunks = [];

        res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        res.on("end", function () {
            var body = Buffer.concat(chunks);
            console.log(body.toString());
        });
    });

    switch (data.role) {

        case 'Member':

            var temp_id = '0099e46e-9cc3-4e48-ae80-bfaa24a89750';

            break;
        case 'Broker':

            var temp_id = 'fdbb45df-4713-4b4c-a2c1-4aec5cb5ab15';

            break;
        case 'Client':

            var temp_id = '26f72939-5099-4b76-819e-24e94001c1d6';

            break;
        case 'Dealer':

            var temp_id = '6bf810af-0c32-49d9-8bff-85cf22c5f056';

            break;

    }

    console.log("tid hrer ===" + temp_id);
    console.log("api key "+ options["headers"]["authorization"]);

    req.write(JSON.stringify({
        template_id: temp_id, personalizations:
            [{
                to: [{ email: data.email, name: data.name }],
                "substitutions": {
                    "%company%": data.companyname,
                    "%fname%": data.fname,
                    "%lname%": data.lname,
                    "%email%": data.email,
                    "%username%": data.name,
                    "%address1%": data.address1,
                    "%address2%": data.address2,
                    "%state%": data.state,
                    "%country%": data.country,
                    "%city%": data.city,
                    "%contact%": data.contact,
                    "%pincode%": data.pincode,
                    "%uid%": data.uid
                },
                subject: 'Welcome to PCEX.'
            }],
        from: { email: 'pcex@panaesha.com', name: 'Team PCEX' },
        reply_to: { email: 'vishwassingh910@gmail.com', name: 'Sam Smith' },
        subject: '',
        content:
            [{
                type: 'text/html',
                value: '<html><p>Hello</p></html>'
            }]
    }));
    req.end();
    callback(null, "Mail Sent.")

}







exports.Mail = Mail;
