var Promise = require('promise');
var request = require("request");
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";


var NewsFeeds = function () { };

//=========================News Feed

NewsFeeds.prototype.Feeds = function (callback) {


    var url = "  http://www.fintechcryptonews.com/api/get_posts/";

    request({
        url: url,
        json: true
    }, function (error, response, body) {

        if (!error && response.statusCode === 200) {
            callback(null, body["posts"])
        } else {

            callback(error, null);
        }
    })
}


exports.NewsFeeds = NewsFeeds; 