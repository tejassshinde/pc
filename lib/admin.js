var Promise = require('promise');

// database connection
var MSql = require('./../routes/server').MSql; // MariaDb Object
var Server = require('./../routes/server').Srv; // Server Datatype
var _server = new Server()
var panaesha_db = new MSql("panaesha");

var Admins = function () { };

//=========================Login
Admins.prototype.login = function (data, callback) {
        var query = `call login("${data.username}","${data.password}")`
        console.log(query)
        panaesha_db.query(query, function (err, rows) {
                console.log("Error Here", err)
                console.log("Data", rows)
                callback(err, rows)
        })
}

//---------------------------------------------------------------------------------------------------------------

//=========================Add Member

Admins.prototype.newMember = function (data, callback) {
        var query = `call newMember("${data.sid}","${data.companyname}","${data.fname}","${data.lname}",
        "${data.address1}","${data.address2}"
        ,"${data.city}","${data.state}","${data.pincode}","${data.country}"
        ,"${data.contact}","${data.email}",
     "${data.pan}" , "${data.aadhar}" , "${data.gst}" ,
     "${data.swift}" , "${data.bank}" , "${data.bank_ac_no}" , "${data.ifsc}",
     "${data.fcc}" , "${data.btc}" , "${data.eth}"
        )`
        console.log(query)

        panaesha_db.query(query, function (err, rows) {
                console.log(err)
                console.log(rows)
                callback(err, rows)
        })
}


//=========================Member list

Admins.prototype.member_list = function (data, callback) {
        var query = `call member_list("${data.sid}")`
        console.log(query)

        panaesha_db.query(query, function (err, rows) {
                console.log(err)
                console.log(rows)
                callback(err, rows)
        })
}

//=========================get wallet

Admins.prototype.getWallet = function (data, callback) {
        var query = `call getWallet("${data.sid}")`
        console.log(query)

        panaesha_db.query(query, function (err, rows) {
                console.log("error",err);
                console.log("rows",rows)

                if(!err && rows && rows[0] && !(rows[0][0] && parseInt(rows[0][0]["wdis_status"]) <=0) )
                {
                        query = `call getWalletNames()`;
                        panaesha_db.query(query, function (err, walletNames) 
                        {
                                if(err)
                                {
                                        callback(err, rows)   
                                }
                                else
                                {
                                        walletNames=walletNames[0].slice(0,walletNames[0].length);
                                        console.log("all ok     ",walletNames);
                                        var newRows = [];
                                        var raw=rows[0][0];
        
                                        for ( var wallet of walletNames ) 
                                        {
                                                var x=wallet["name"];
                                                var a=x+'';
                                                var b=x+'_hold';
                                                
                                                
                                                newRows.push({name:x.toUpperCase(),total:(+raw[a])+(+raw[b]),main:(+raw[a]),hold:(+raw[b])});
                                        }
        
                                        rows[0]=newRows; 
                                        callback(err, rows)
                                }
                               

                        })
                }
                else{
                        callback(err, rows)
                }
                

                
        })
}




//---------------------------------------------------------------------------------------------------------------
//=========================Add Symbol

Admins.prototype.newSymbol = function (data, callback) {
        var query = `call newSymbol("${data.sid}","${data.symbol_name}","${data.market}","${data.moq}",
        "${data.tick_size}","${data.margin}","${data.margin_type}")`
        console.log(query)

        panaesha_db.query(query, function (err, rows) {
                console.log(err)
                console.log(rows)
                callback(err, rows)
        })
}

//=========================Symbol list

Admins.prototype.symbol_list = function (data, callback) {
        var query = `call symbol_list("${data.sid}")`
        console.log(query)

        panaesha_db.query(query, function (err, rows) {
                console.log(err)
                console.log(rows)
                callback(err, rows)
        })
}

//=========================Logout

Admins.prototype.logout = function (data, callback) {
        var query = `call logout("${data.sid}")`
        console.log(query)

        panaesha_db.query(query, function (err, rows) {
                console.log(err)
                console.log(rows)
                callback(err, rows)
        })
}



//=========================Add payInOut

Admins.prototype.newPayInOut = function (data, callback) {
        var query = `call newpayInOut("${data.sid}","${data.pDate}","${data.clientCode}","${data.instrumentType}",
        "${data.instrumentNo}","${data.instrumentDate}","${data.amount}","${data.currency}","${data.pMode}")`
        console.log(query)

        panaesha_db.query(query, function (err, rows) {
                console.log(err)
                console.log(rows)
                callback(err, rows)
        })
}

//=========================payIn List

Admins.prototype.payIn_list = function (data, callback) {
        var query = `call payIn_list("${data.sid}")`
        console.log(query)

        panaesha_db.query(query, function (err, rows) {
                console.log(err)
                console.log(rows)
                callback(err, rows)
        })
}

//=========================payOut List

Admins.prototype.payOut_list = function (data, callback) {
        var query = `call payOut_list("${data.sid}")`
        console.log(query)

        panaesha_db.query(query, function (err, rows) {
                console.log(err)
                console.log(rows)
                callback(err, rows)
        })
}

//=========================Client Ledger

Admins.prototype.clientLedger = function (data, callback) {
        var query = `call client_ledger("${data.sid}","${data.cCode}")`
        console.log(query)

        panaesha_db.query(query, function (err, newrow) {
                console.log("ClientLedger Data =======")
                console.log(err)
                console.log(newrow)

                if (newrow[0][0]["wdis_status"]) {
                //if (newrow[0][0]["wdis_status"] == 0) {
                        callback(err,newrow );
                } else {


                        rows = newrow[0]
                        var ledger = {};
                        var prevCurrency = "";
                        var balance = 0;
                        // addig computed balance

                        var temp = [];

                        for (var i = 0; i < rows.length; i++) {
                                if (rows[i]["Currency"] !== prevCurrency) {
                                        balance = 0;
                                        prevCurrency = rows[i]["Currency"];

                                        if (i > 0) {
                                                ledger[prevCurrency] = temp
                                        }
                                        balance = rows[i]["Credit"] - rows[i]["Debit"];
                                        rows[i]["balance"] = balance
                                        prevCurrency = rows[i]["Currency"];
                                        temp = []
                                        temp.push(rows[i]);
                                        // console.log(rows[i]);
                                }
                                else {
                                        balance = rows[i]["Credit"] - rows[i]["Debit"];
                                        rows[i]["balance"] = balance
                                        temp.push(rows[i]);
                                }

                                if (i == rows.length - 1)
                                        callback(err, [ledger]);
                        }
                }
        })
}

// Check usename

Admins.prototype.checkUsername = function (data, callback) {
        var query = `call check_usename("${data.username}")`
        // console.log(query)

        panaesha_db.query(query, function (err, rows) {
                console.log(err)
                console.log(rows)
                callback(err, rows)
        })
}

// Check Email

Admins.prototype.checkEmail = function (data, callback) {
        var query = `call check_email("${data.email}")`
        // console.log(query)

        panaesha_db.query(query, function (err, rows) {
                console.log(err)
                console.log(rows)
                callback(err, rows)
        })
}




Admins.prototype.startLoadServer = function (data, callback) {
        var query = `call getSymbols()`;
        //console.log(query)

        
        panaesha_db.query(query, function (err, rows) 
        {

                console.log("error",err);
                console.log("rows",rows);

                var raw=[];
                var orderType='';
                var fraction=0;
                var index=0;
                var symbol_id=1;
                var qty=0;
                raw=rows[0];
                console.log("raw is :",raw);
                var i=0;
                setInterval(function()
                {
                        i++;
                        if(i>9999999)
                        {
                                i=0;
                        }

                        if(i%2 == 0)
                        {
                                orderType='Buy';
                                //fraction=Math.random() * (1.15 - 1.0) + 1.0;
                        }
                        else
                        {
                                orderType='Sell';
                                //fraction=Math.random() * (1.00 - 0.95) + 0.95;
                        }

                        fraction=Math.random() * (1.05 - 0.95) + 0.95;

                     index=Math.floor(Math.random() * raw.length);
                     //console.log("index is:",index);
                     symbol_id=raw[index]["id"];
                     qty=Math.random() * (raw[index]["loadMaxQty"] - 0.01) + 0.01;


                     query = `call newOrderTestingServer("${symbol_id}","${orderType}","${qty}","${fraction}")`;

                     panaesha_db.query(query, function (err, r) 
                     {
                        
                        })

                },1500); 


                

        })



}




exports.Admins = Admins;

