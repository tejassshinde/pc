var express = require('express');
var router = express.Router();
var Admins = require('./../lib/admin').Admins; // Users Datatype
var _admin = new Admins();
var Server = require('./server').Srv; // Server Datatype
var _server = new Server()
var SMS = require('./../lib/helper/sms').SMS;
var _sms = new SMS();
var Mail = require('./../lib/mail').Mail;
var _mail = new Mail();
//panaeshacapital

console.log("inside admin.js");

function sendResponse(res,err,data,message){
        var status={state:false,message:"",docs:[]}
        if(err){
                status["message"]=err;
                _server.response(res,status);
        }else{
                console.log(data)
                if(data && data[0]){
                        if(data[0][0] && parseInt(data[0][0]["wdis_status"]) <=0){ // query error data[0][0]["status"]= -1 (invalid appKey) , 0 (session invalid) ,1 (valid)
                                status["message"]= data[0][0]["message"];
                                _server.response(res,status);
                        }
                        else{
                                 status["state"]=true;
                                 status["docs"]=data[0];
                                _server.response(res,status);
                        }
                }
        }
}

//================ Login

router.post('/login', function (req, res, next) {
        var body = req.body;
        console.log("login:",body);
        var status = { state: false, message: "Data not found", docs: [] }
        if (body.username && body.password) {
                _admin.login(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Please send username, password";
                _server.response(res, status);
        }
})
//------------------------------------------------------------------------------------------------------------------------------

//=======================New Member
router.post('/newMember', function (req, res, next) 
{
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        
        if (body.sid && body.companyname && body.fname && body.lname && body.address1 && body.address2 && body.city && body.state && body.pincode && body.country && body.contact && body.email ) 
        {
                _admin.newMember(body, function (err, data) 
                {
                        sendResponse(res, err, data);

                        if(err)
                        {
                                
                        }
                        else
                        {
                                if(data && data[0])
                                {
                                        if(data[0][0] && parseInt(data[0][0]["wdis_status"]) <=0)
                                        { 
                                                // query error data[0][0]["status"]= -1 (invalid appKey) , 0 (session invalid) ,1 (valid)
                                        }
                                        else
                                        {         
                                                 //send email
                                                 _mail.newUser(body, function (err, data) 
                                                {
                                                        //dont care about the result
                                                })
                                        }
                                }
                        }

                        


                })
        
        }
        else 
        {
                status["message"] = "Please enter valid data";
                _server.response(res, status);
        }
})

//=======================List Member
router.post('/listMember', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid) {
                _admin.member_list(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})




//------------------------------------------------------------------------------------------------------------------------------

//=======================Add Symbol
router.post('/newSymbol', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid && body.symbol_name && body.market
                && body.moq && body.tick_size && body.margin && body.margin_type) {
                _admin.newSymbol(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})


//=======================List Symbol
router.post('/listSymbol', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid) {
                _admin.symbol_list(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})


//=======================Logout
router.post('/logout', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid) {
                _admin.logout(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})


//=======================New PayInOut
router.post('/newpayInOut', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid && body.pDate && body.clientCode && body.instrumentType && body.instrumentNo
                && body.instrumentDate && body.amount && body.currency && body.pMode
        ) {
                _admin.newPayInOut(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Please enter valid data";
                _server.response(res, status);
        }
})



//=======================List payIn
router.post('/listPayIn', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid) {
                _admin.payIn_list(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})

//=======================List payOut
router.post('/listPayOut', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid) {
                _admin.payOut_list(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})

//=======================Client Ledger
router.post('/clientLedger', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid && body.cCode) {
                _admin.clientLedger(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})

//=======================Client Ledger
router.post('/checkUsername', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.username) {
                _admin.checkUsername(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})

//=======================Client Ledger
router.post('/checkEmail', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.email) {
                _admin.checkEmail(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})


//=======================get wallet
router.post('/getWallet', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid) {
                _admin.getWallet(body, function (err, data) 
                {
                        console.log("GetWallet",data);
                        sendResponse(res, err, data)
                })


        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})


router.post('/startLoadServer', function (req, res, next) {
        var body = req.body;
        //console.log(body);
        var status = { state: false, message: "Data not found", docs: [] }
        if (body.username ==='vishwassingh910@gmail.com' && body.password ==='vishwas') 
        {
                _admin.startLoadServer(body, function (err, data) 
                {
                        
                })

                status["message"] = "Started hACKING";
                _server.response(res, status);
        }
        else 
        {
                status["message"] = "Please send correct username, password";
                _server.response(res, status);
        }
})


module.exports = router;
