var express = require('express');
var router = express.Router();
var Members= require('./../lib/member').Members; // Users Datatype
var _member= new Members();
var Server = require('./server').Srv; // Server Datatype
var _server=new Server()
var SMS= require('./../lib/helper/sms').SMS;
var _sms = new  SMS();




function sendResponse(res,err,data,message){
        var status={state:false,message:"",docs:[]}
        if(err){
                status["message"]=err;
                _server.response(res,status);
        }else{
                console.log(data)
                if(data && data[0]){
                        if(data[0][0] && parseInt(data[0][0]["wdis_status"]) <=0){ // query error data[0][0]["status"]= -1 (invalid appKey) , 0 (session invalid) ,1 (valid)
                                status["message"]= data[0][0]["message"];
                                _server.response(res,status);
                        }
                        else{
                                 status["state"]=true;
                                 status["docs"]=data[0];
                                _server.response(res,status);
                        }
                }
        }
}
//=======================New Broker
router.post('/newBroker', function(req, res, next) {
        var body=req.body;
         console.log(body);
        var status={state:false,message:"",docs:[]}
       if( body.sid && body.companyname && body.fname && body.lname && body.address1
        && body.address2 && body.city && body.state && body.pincode
        && body.country && body.contact && body.email){
                _member.newBroker(body,function(err,data){
                        sendResponse(res,err,data)
                })
        }
        else{
                status["message"]="Please enter valid data";
                _server.response(res,status);
        }
})

//=======================List Broker
router.post('/listBroker', function(req, res, next) {
        var body=req.body;
         console.log(body);
        var status={state:false,message:"",docs:[]}
       if( body.sid ){
                _member.broker_list(body,function(err,data){
                        sendResponse(res,err,data)
                })
        }
        else{
                status["message"]="Invalid SessionId";
                _server.response(res,status);
        }
})


//=======================New RM
router.post('/newRm', function(req, res, next) {
        var body=req.body;
         console.log(body);
        var status={state:false,message:"",docs:[]}
       if( body.sid && body.companyname && body.fname && body.lname && body.address1
        && body.address2 && body.city && body.state && body.pincode
        && body.country && body.contact && body.email ){
                _member.newRm(body,function(err,data){
                        sendResponse(res,err,data)
                })
        }
        else{
                status["message"]="Please enter valid data";
                _server.response(res,status);
        }
})

//=======================List RM
router.post('/listRm', function(req, res, next) {
        var body=req.body;
         console.log(body);
        var status={state:false,message:"",docs:[]}
       if( body.sid ){
                _member.listRm(body,function(err,data){
                        sendResponse(res,err,data)
                })
        }
        else{
                status["message"]="Invalid SessionId";
                _server.response(res,status);
        }
})
//=======================List RM
router.post('/verifyRmCode', function(req, res, next) {
        var body=req.body;
         console.log(body);
        var status={state:false,message:"",docs:[]}
       if( body.rm_code ){
                _member.verifyRmCode(body,function(err,data){
                        sendResponse(res,err,data)
                })
        }
        else{
                status["message"]="Invalid Data";
                _server.response(res,status);
        }
})

//=======================
router.post('/newTransaction', function(req, res, next) {
        var body=req.body;
         console.log(body);
        var status={state:false,message:"",docs:[]}
       if( body.rm_code && body.transactionDetails){
                _member.newTransaction(body,function(err,data){
                        sendResponse(res,err,data)
                })
        }
        else{
                status["message"]="Invalid Data";
                _server.response(res,status);
        }
})







module.exports=router;
