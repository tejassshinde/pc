var express = require('express');
var router = express.Router();
var Brokers= require('./../lib/broker').Brokers; // Users Datatype
var _broker= new Brokers();
var Server = require('./server').Srv; // Server Datatype
var _server=new Server()
var SMS= require('./../lib/helper/sms').SMS;
var _sms = new  SMS();
var Mail = require('./../lib/mail').Mail;
var _mail = new Mail();


//
function sendResponse(res,err,data,message){
        var status={state:false,message:"",docs:[]}
        if(err){
                status["message"]=err;
                _server.response(res,status);
        }else{
                //console.log(data)
                if(data && data[0]){
                        if(data[0][0] && parseInt(data[0][0]["wdis_status"]) <=0){ // query error data[0][0]["status"]= -1 (invalid appKey) , 0 (session invalid) ,1 (valid)
                                status["message"]= data[0][0]["message"];
                                _server.response(res,status);
                        }
                        else{
                                 status["state"]=true;
                                 status["docs"]=data[0];
                                _server.response(res,status);
                        }
                }
        }
}
//=======================New Client
router.post('/newClient', function(req, res, next) 
{
        var body=req.body;
        console.log("newClient:",body);
        var status={state:false,message:"",docs:[]};

       if( body.sid && body.companyname && body.fname && body.lname && body.address1 && body.address2 && body.city && body.state && body.pincode && body.country && body.contact && body.email )
       {
                _broker.newClient(body,function(err,data)
                {
                        console.log("newClient error:",err);
                        console.log("newClient data:",data);
                        sendResponse(res,err,data);

                        var ok= (!err) && data && data[0] && !(data[0][0] && parseInt(data[0][0]["wdis_status"]) <=0);

                        if(ok === true)
                        {
                                console.log("sending mail");
                                //send email
                                _mail.newUser(body, function (err, data) 
                                {
                                        console.log("Send Mail error:",err);
                                        console.log("Send Mail data:",data);
                                        //dont care about the result
                                })
                        }

                })
                /*
                if( body.sid && body.companyname && body.fname && body.lname && body.address1 && body.address2 && body.city && body.state && body.pincode && body.country && body.contact && body.email )
       {
               console.log("11");
                _broker.newClient(body, function (err, data) 
                {

                        console.log("newClient error:",err);
                        console.log("newClient data:",data);
                        sendResponse(res, err, data);

                        var ok= (!err) && data && data[0] && !(data[0][0] && parseInt(data[0][0]["wdis_status"]) <=0);

                        if(ok === true)
                        {
                                //send email
                                _mail.newUser(body, function (err, data) 
                                {
                                        console.log("Send Mail error:",err);
                                        console.log("Send Mail data:",data);
                                        //dont care about the result
                                })
                        }
                })
        }
        else
        {
                status["message"]="Please enter valid data";
                _server.response(res,status);
        }


        */



        }
        else
        {
                status["message"]="Please enter valid data";
                _server.response(res,status);
        }

        


})

//=======================List Client
router.post('/listClient', function(req, res, next) {
        var body=req.body;
         console.log(body);
        var status={state:false,message:"",docs:[]}
       if( body.sid){
                _broker.client_list(body,function(err,data){
                        sendResponse(res,err,data)
                })
        }
        else{
                status["message"]="Invalid SessionID";
                _server.response(res,status);
        }
})

//=======================New Dealer
router.post('/newDealer', function(req, res, next) {
        var body=req.body;
         console.log(body);
        var status={state:false,message:"",docs:[]}
       if( body.sid && body.companyname && body.fname && body.lname && body.address1
        && body.address2 && body.city && body.state && body.pincode
        && body.country && body.contact && body.email){
                _broker.newDealer(body,function(err,data){
                        sendResponse(res,err,data)
                })
        }
        else{
                status["message"]="Please enter valid data";
                _server.response(res,status);
        }
})

//=======================List dealers
router.post('/listDealer', function(req, res, next) {
        var body=req.body;
         console.log(body);
        var status={state:false,message:"",docs:[]}
       if( body.sid){
                _broker.dealer_list(body,function(err,data){
                        sendResponse(res,err,data)
                })
        }
        else{
                status["message"]="Invalid SessionID";
                _server.response(res,status);
        }
})

//=======================Search cCode
router.post('/searchClient', function(req, res, next) {
        var body=req.body;
         console.log(body);
        var status={state:false,message:"",docs:[]}
       if( body.sid, body.cCode){
                _broker.searchClient(body,function(err,data){
                        sendResponse(res,err,data)
                })
        }
        else{
                status["message"]="Invalid SessionID";
                _server.response(res,status);
        }
})







module.exports=router;
