var express = require('express');
var router = express.Router();
var Terminal = require('./../lib/terminal').Terminal; // Users Datatype
var _terminal = new Terminal();
var Server = require('./server').Srv; // Server Datatype
var _server = new Server()
var SMS = require('./../lib/helper/sms').SMS;
var _sms = new SMS();


function sendResponse(res,err,data,message){
        var status={state:false,message:"",docs:[]}
        if(err){
                status["message"]=err;
                _server.response(res,status);
        }else{
                if(data && data[0]){
                        if(data[0][0] && parseInt(data[0][0]["wdis_status"]) <=0){ // query error data[0][0]["status"]= -1 (invalid appKey) , 0 (session invalid) ,1 (valid)
                                status["message"]= data[0][0]["message"];
                                _server.response(res,status);
                        }
                        else{
                                 status["state"]=true;
                                 status["docs"]=data[0];
                                _server.response(res,status);
                        }
                }
        }
}

//=======================List Order
router.post('/listOrder', function (req, res, next) {
        var body = req.body;
        //console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid) {
                _terminal.order_list(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})


//=======================new Order
router.post('/newOrder', function (req, res, next) {
        var body = req.body;
        console.log("new order is : ",body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid && body.symbol_id && body.order_type && body.order_qty &&
                body.order_limit_rate) {
                
                
                _terminal.newOrder(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Enter valid data";
                _server.response(res, status);
        }
})


//=======================Forgot Password
router.post('/ForgotPassword', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.email) {
                _terminal.ForgotPassword(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid Email";
                _server.response(res, status);
        }
})

//=======================Reset Password
router.post('/resetPassword', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.token && body.newPassword) {
                _terminal.resetPassword(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid token";
                _server.response(res, status);
        }
})

//=======================Change Password
router.post('/changePassword', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.oldPassword && body.newPassword && body.sid) {
                _terminal.changePassword(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid Session";
                _server.response(res, status);
        }
})

//=======================List Tick
router.post('/showTick', function (req, res, next) {
        var body = req.body;
//        console.log(body);
        var status = { state: false, message: "", docs: [] }
        try{
        if (body.sid) {
                _terminal.showTick(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
        }catch(e){
               //console.log(e);
        }


})

//=======================List Trade
router.post('/listTrade', function (req, res, next) {
        var body = req.body;
       // console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid) {
                _terminal.listTrade(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})

//=======================net Position
router.post('/netPosition', function (req, res, next) {
        var body = req.body;
       // console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid) {
                _terminal.netPosition(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})

//=======================net Position
router.post('/cancelOrder', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid && body.order_id) {
                _terminal.cancelPending(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})

//============== accept T&C

router.post('/acceptTnc', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid) {
                _terminal.acceptTnc(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Invalid SessionId";
                _server.response(res, status);
        }
})
//=======================new Order
router.post('/newOrderDealer', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid && body.symbol_id && body.order_type && body.order_qty &&
                body.order_limit_rate && body.client_code) {
                _terminal.newOrderDealer(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Enter valid data";
                _server.response(res, status);
        }
})

//===================== check Client Code

router.post('/checkClientCode', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.client_code) {
                _terminal.checkClientCode(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        
})

//======================= brokerage Lsit
router.post('/brokerageList', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.sid ) {
                _terminal.brokerageList(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Enter valid data";
                _server.response(res, status);
        }
})

//======================= brokerage Lsit
router.post('/tOtpAuth', function (req, res, next) {
        var body = req.body;
        console.log(body);
        var status = { state: false, message: "", docs: [] }
        if (body.pcexKey && body.contact ) {
                _terminal.tOtpAuth(body, function (err, data) {
                        sendResponse(res, err, data)
                })
        }
        else {
                status["message"] = "Enter contact no.";
                _server.response(res, status);
        }
})




module.exports = router;
