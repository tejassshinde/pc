var express = require('express');
var router = express.Router();
var Mail = require('./../lib/mail').Mail;
var _mail = new Mail();
var Server = require('./server').Srv; // Server Datatype
var _server = new Server()






function sendResponse(res, err, data, message) {
    var status = { state: false, message: "", docs: [] }
    if (err) {
        status["message"] = err;
        _server.response(res, status);
    } else {
        console.log(data)
        if (data && data[0]) {
            if (data[0][0] && parseInt(data[0][0]["wdis_status"]) <= 0) { // query error data[0][0]["status"]= -1 (invalid appKey) , 0 (session invalid) ,1 (valid)
                status["message"] = data[0][0]["message"];
                _server.response(res, status);
            }
            else {
                status["state"] = true;
                status["docs"] = data[0];
                //_server.response(res,status);
            }
        }
    }
}
//=================================Getting values from Reset Password
router.get('/reset', function (req, res, next) {
    // var body = req.body;
    var qs = req.query;

    console.log(qs);
    var status = { state: false, message: "", docs: [] }
    if (qs.e && qs.i) {
        _mail.validate(qs, function (err, data) {

            sendResponse(res, err, data);
            console.log("++++++++" + data[0][0]["wdis_status"]);
            if (data[0][0] && parseInt(data[0][0]["wdis_status"]) == 1) {
                res.redirect('http://pcex.ditscentre.in/#/resetPassword');
            }




        })
    } else {
        status["message"] = "invalid user";
        _server.response(res, status);
    }

});

//=================================Getting values from Reset Password
router.get('/verifyEmail', function (req, res, next) {
    // var body = req.body;
    var qs = req.query;

    console.log(qs);
    var status = { state: false, message: "", docs: [] }
    if (qs.e && qs.i && qs.r) {
        _mail.validate(qs, function (err, data) {

            sendResponse(res, err, data);
            console.log("++++++++" + data[0][0]["wdis_status"]);
            if (data[0][0] && parseInt(data[0][0]["wdis_status"]) == 1) {
                if (qs.r == "Client" || qs.r == "Dealer") {

                    res.redirect('http://pcex.ditscentre.in/terminal/#/login');
                }
                else {
                    res.redirect('http://pcex.ditscentre.in/#/login');

                }
            }else{
                if (qs.r == "Client" || qs.r == "Dealer") {

                    res.redirect('http://pcex.ditscentre.in/terminal/#/login');
                }
                else {
                    res.redirect('http://pcex.ditscentre.in/#/login');

                }
            }

        })
    } else {
        status["message"] = "invalid user";
        _server.response(res, status);
    }

});

//=======================Signup mail
router.post('/newUser', function (req, res, next) {
    var body = req.body;
    console.log(body);
    var status = { state: false, message: "", docs: [] }
    if (body) {
        _mail.newUser(body, function (err, data) {
            sendResponse(res, err, data)
        })
    }
    else {
        status["message"] = "Please enter valid data";
        _server.response(res, status);
    }
})

//=======================Signup mail
router.post('/forgotPassword', function (req, res, next) {
    var body = req.body;
    console.log(body);
    var status = { state: false, message: "", docs: [] }
    if (body) {
        _mail.forgotPassword(body, function (err, data) {
            sendResponse(res, err, data)
        })
    }
    else {
        status["message"] = "Please enter valid data";
        _server.response(res, status);
    }
})






module.exports = router;


